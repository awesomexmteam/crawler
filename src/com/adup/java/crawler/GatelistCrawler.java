package com.adup.java.crawler;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class GatelistCrawler {
	private static final Logger logger = LogManager.getLogger(GatelistCrawler.class);
	private static final String DUPLICATE_MySQL = "23000";
//	private PreparedStatement statement = null;
	final int vpnGateService = 5;
//	final int vpnAccount = 1;
//	private static int maxVpnUser, minVpnUser;

	public static void main(String[] args) {
		Properties prop = new Properties();
    	InputStream input = null;

    	try {
    		String filename = "config.properties";
    		input = GatelistCrawler.class.getClassLoader().getResourceAsStream(filename);
    		if(input==null){
    	            System.out.println("Sorry, unable to find " + filename);
    		    return;
    		}
			// load a properties file
			prop.load(input);

			// get the property value and print it out
//			System.out.println(prop.getProperty("dbuser"));
//			System.out.println(prop.getProperty("dbpassword"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		String url = prop.getProperty("url"); //"jdbc:mysql://localhost:3306/gcdb2";
		String username = prop.getProperty("dbUser"); //"gcuser";
		String password = prop.getProperty("dbPassword");//"gcuser@Adon";

		MyDataSource.initialize(url, username, password);
//		maxVpnUser = Integer.valueOf(prop.getProperty("maxVpnUser"));
//		minVpnUser = Integer.valueOf(prop.getProperty("minVpnUser"));
		
		GatelistCrawler obj = new GatelistCrawler();
		obj.runGateList();

	}

	private PreparedStatement createInsertVPNStatement(Connection connection, String ip, String profile, String countryCode,
			String ipcountryCode, String ipRegionCode, String ipRegionName, String ipcity, String ipZipCode, 
			double ipLat, double ipLong, int ipMetroCode,  String iptz) throws SQLException {
		String insertQuery = "insert into vpn (server_ip, service_id, config, "
				+ "country_code, ip_country_code, region_code, "
				+ "region_name, city, zip_code, latitude, "
				+ "longitude, metro_code, tz) values (?,?,?,?,?,?,?,?,?,?,?,?,?) "
				+ "ON DUPLICATE KEY UPDATE activated = 1, refresh_time = current_timestamp";
		PreparedStatement statement = connection.prepareStatement(insertQuery, new String[]{"id"});
		statement.setString(1, ip);
		statement.setInt(2, vpnGateService);
		statement.setString(3, profile);
		statement.setString(4, countryCode);
		statement.setString(5, ipcountryCode);
		statement.setString(6, ipRegionCode);
		statement.setString(7, ipRegionName);
		statement.setString(8, ipcity);
		statement.setString(9, ipZipCode);
		statement.setDouble(10, ipLat);
		statement.setDouble(11, ipLong);
		statement.setInt(12, ipMetroCode);
		statement.setString(13, iptz);
		return statement;
	}

//	private PreparedStatement createInsertVPNAccountAssignmentStatement(Connection connection, int vpnId) throws SQLException {
//		String insertQuery = "insert into account_assignment (account_id, vpn_id) values (?,?)";
//		PreparedStatement statement = connection.prepareStatement(insertQuery);
//		statement.setInt(1, vpnAccount);
//		statement.setInt(2, vpnId);
//		return statement;
//
//	}

	private ArrayList<String> sendHttpGetRequest(String link) {
		HttpURLConnection conn;
		BufferedReader in = null;
		int result = -1;
		String returnString = "";

		try {
			URL url = new URL(link);
			if (link.startsWith("https")) {
				conn = (HttpsURLConnection) url.openConnection();
			} else {
				conn = (HttpURLConnection) url.openConnection();
			}
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(15000);
			result = conn.getResponseCode();
			if (result >= 400) {
				if (conn.getErrorStream()!=null) 
					in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));

			} else {
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			}
			StringBuilder stringBuilder = new StringBuilder();
			String line;
			if (in!=null) {
				while ((line = in.readLine()) != null) {
					stringBuilder.append(line);
				}
			}

			returnString = stringBuilder.toString();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			logger.error("Error firing postback link: {}", link);
			// e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		ArrayList<String> returned = new ArrayList<>();
		returned.add(String.valueOf(result));
		returned.add(returnString);
		return returned;
	}
	public void runGateList() {
		InputStream profile_input = null;
		// SharedPreferences sharedPref =
		// PreferenceManager.getDefaultSharedPreferences(context);
		final int vpnBase64Col = 14;
		final int ipCol = 1;
		final int countryCodeCol = 6;
		final String link_csv = "http://www.vpngate.net/api/iphone/";
		final int skipLines = 2;
		URL url;
		HttpURLConnection httpConnection = null;

		String csvLine;
		try {
			url = new URL(link_csv);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setConnectTimeout(15000);
			httpConnection.setReadTimeout(60000);
			profile_input = httpConnection.getInputStream();
			BufferedReader csvReader = new BufferedReader(new InputStreamReader(profile_input));
			int lineNum = 0;
			int insertCount = 0;
			int totalCount = 0;
			int duplicateCount = 0;
			try ( Connection dbConnection = MyDataSource.getInstance().getConnection();) {

				while ((csvLine = csvReader.readLine()) != null) {
					lineNum++;
					if (lineNum > skipLines) {
						String[] rowData = csvLine.split(",");
						if (rowData.length > vpnBase64Col) {
							Matcher matcher = Pattern.compile("\\b(?:\\d{1,3}\\.){3}\\d{1,3}\\b").matcher(rowData[ipCol]);
							if (matcher.matches()) {
								totalCount ++;
								String ip = rowData[ipCol];
								String ipcountryCode=null, ipcity=null, iptz=null;
								String ipRegionCode=null, ipRegionName=null, ipZipCode=null;
								double ipLat=0, ipLong=0;
								int ipMetroCode=0;
								try {
									String freeGeoIP = "http://freegeoip.net/json/" + ip;
									ArrayList<String> httpResult = sendHttpGetRequest(freeGeoIP);
									int freeGeoResponseCode = Integer.parseInt(httpResult.get(0));
									if (freeGeoResponseCode==200) {
										String freeGeoResponseContent = httpResult.get(1);
	//									logger.info("content {}", freeGeoResponseContent);
										JSONParser jsonParser = new JSONParser();
										JSONObject jsonObject = (JSONObject) jsonParser.parse(freeGeoResponseContent);
										ipcountryCode = (String) jsonObject.get("country_code");
										ipRegionCode = (String) jsonObject.get("region_code");
										ipRegionName = (String) jsonObject.get("region_name");
										ipZipCode = (String) jsonObject.get("zip_code");
										iptz = (String) jsonObject.get("time_zone");
										ipLat = doubleValue(jsonObject.get("latitude"));
										ipLong = doubleValue(jsonObject.get("longitude"));
										ipMetroCode = intValue(jsonObject.get("metro_code")); 
										ipcity = (String) jsonObject.get("city");
									} else {
										logger.error("freeGeocode: ", freeGeoResponseCode);
									}
									
								} catch (Exception ignored) {
									logger.error("freeGeo exception", ignored);
									
								}
								String countryCode = rowData[countryCodeCol].toUpperCase();
								String profile = cleanUpBase64( rowData[vpnBase64Col]);
//								logger.info("IP : {}, CountryCode: {}", ip, countryCode);
								try ( PreparedStatement statement = createInsertVPNStatement(dbConnection, ip, profile, countryCode, ipcountryCode, ipRegionCode, ipRegionName, ipcity, ipZipCode, ipLat, ipLong, ipMetroCode, iptz)) {
									int result = statement.executeUpdate();
									if (result==1)
										insertCount ++;
									else if (result==2)
										duplicateCount ++;
									
								} catch (SQLException e) {
									logger.error(e.getMessage(), e);
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.error("Failed to open connection", e);
			}
			logger.info("Crawled total: {}. Updated: {}. Inserted: {}", totalCount, duplicateCount, insertCount);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (profile_input != null)
				try {
					profile_input.close();
				} catch (IOException e) {
					logger.error(e.getMessage(), e);

				}
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
		}
	}

	  private String cleanUpBase64(String encodedString) {
		// TODO Auto-generated method stub
		  byte[] rawFile = Base64.decodeBase64(encodedString);
      BufferedReader isr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(rawFile)));
      StringBuilder stringBuilder = new StringBuilder();
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
      String line;
      try {
			while ((line = isr.readLine())!=null) {
				if (!line.startsWith("#")) {
					stringBuilder.append(line).append(System.lineSeparator());
				}
			}
			outputStreamWriter.write(stringBuilder.toString());
			outputStreamWriter.flush();
			outputStreamWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
	  }


	private static double doubleValue(Object value) {
		    return (value instanceof Number ? ((Number)value).doubleValue() : -1.0);
		  }
	  private static int intValue(Object value) {
		    return (value instanceof Number ? ((Number)value).intValue() : -1);
		  }
}
