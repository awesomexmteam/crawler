package com.adup.java.crawler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MyDataSource {
	public static String DB_URL;
	public static String DB_USER;
	public static String DB_PASSWORD;

	public static boolean initialize(String url, String user, String password) {
		DB_URL = url;
		DB_USER = user;
		DB_PASSWORD = password;
		return true;

	}
	
	private static class DataSourceHolder {
		private static final MyDataSource INSTANCE = new MyDataSource();
	}

	public static MyDataSource getInstance() {
		return DataSourceHolder.INSTANCE;
	}

	public Connection getConnection() {
		try {
			Connection connection = DriverManager
			.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			return connection;
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		return null;
	}


}